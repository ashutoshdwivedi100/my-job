import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Home from "./view/Home/Home"
// import Login from "./view/Login/Login"
import PageNotFound from "./view/PageNotFound/PageNotFound"
import Navbar from './view/Navbar/Navbar';
// import SignUp from './view/SignUp/SignUp';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
            <Route path="/" exact component={Home} />
            {/* <Route path="/login" exact component={Login} />
            <Route path="/sign-up" exact component={SignUp} /> */}
            <Route component={PageNotFound} />
          </Switch>
      </Router>
    </div>
  );
}

export default App;
