import React from 'react'

const PageNotFound = () => {
    return (
        <div style={{height: "80vh", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", color: "#1A253C"}}>
            <h1 style={{fontSize: "4rem", margin: "0"}}>404</h1>
            <p style={{fontSize: "2rem"}}>Page Not Found</p>
        </div>
    )
}

export default PageNotFound
